# 75.06/95.58 Organización de Datos
# Competencia de Machine Learning

# Trabajo Práctico 2 -  Segundo Cuatrimestre de 2018


---

## Integrantes - Grupo 46 (Grupo de oyentes)

 - Álvarez González, José Mariano
 - Bernabó, Guillermo
 - Bertin, Natacha
 - Huerta San Martín, Marcelo Gustavo

---

## Enunciado completo

https://docs.google.com/document/d/1nI5ZKpm6DfCUy1SeJ1GXm29rxwVTpeFhmLh5SCuFuYg/edit?usp=sharing


##

El segundo trabajo práctico es una competencia de Machine Learning en donde cada grupo
debe intentar determinar, para cada usuario presentado, cuál es la probabilidad de que ese
usuario realice una conversión en Trocafone en un periodo determinado.

El link a la competencia es https://www.kaggle.com/t/f477d6cc8ce34161a33bcc02ad055912


## Archivos

Archivo | Shape
--|--
events_up_to_01062018.csv | 2.34m x 23
labels_training_set.csv | 19.4k x 2
trocafone_kaggle_test.csv | 19.4k x 1

 Desde Kaggle https://www.kaggle.com/c/trocafone/data
 
 Desde Google drive https://drive.google.com/file/d/1kQujhvOKAU4EzhaYDbgquf_XKFt9sHMy/view?usp=sharing




> NOTA: En el link de google **no está** el archivo "trocafone_kaggle_test.csv" que se requiere para hacer el submit a Kaggle y que si está en kaggle


## Requisitos para la entrega del TP2:

- El TP debe ser programado en Python o R
- Debe entregarse una carpeta con el informe de algoritmos probados, algoritmo final
utilizado, transformaciones realizadas a los datos, feature engineering, etc.
- La entrega debe incluir también un link a github con el informe presentado en pdf, y todo
el código.
- El grupo debe presentar el TP en una computadora en la fecha indicada por la cátedra,
el TP debe correr en un lapso de tiempo razonable (inferior a 1 hora) y generar un
submission válido que iguale el mejor resultado obtenido por el grupo en Kaggle.

## El TP2 se va a evaluar en función del siguiente criterio:
- Cantidad de trabajo (esfuerzo) del grupo: ¿Probaron muchos algoritmos? ¿Hicieron un
buen trabajo de pre-procesamiento de los datos y feature engineering?
- Resultado obtenido en Kaggle (obviamente cuanto mejor resultado mejor nota)
- Presentación final del informe, calidad de la redacción, uso de información obtenida en
el TP1, conclusiones presentadas.
- Performance de la solución final.

## IMPORTANTE

> Agregue un archivo **.gitignore** al repositorio para que **NO** envie al repositorio de datos los archivos CSV  ni los checkpoint de los notebooks. 
>
> Mariano